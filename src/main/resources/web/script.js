mapboxgl.accessToken = 'pk.eyJ1IjoiZGFubnliMTIzIiwiYSI6ImNqMGljZ256dzAwMDAycXBkdWxwbDgzeXYifQ.Ck5P-0NKPVKAZ6SH98gxxw';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v9',
    center: [-96, 37.8],
    zoom: 3
});

map.on('load',
    function() {
		console.log("Start to import schools and pupils");
        fetchSchools();

        function fetchSchools() {
            $.get("/webapi/schools", function(data) {
                window.schools = [];
                data.forEach(function(school) {
                    window.schools.push({
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                            	school.lat,
                            	school.lon
                            ]
                        },
                        "properties": {
                            "title": "School ID " + school.id,
                            "icon": "school"
                        }
                    })
                });
                console.log("schools load was performed");
                fetchPupils();
            });
        }

        function fetchPupils() {
            $.get("/webapi/pupils", function(data) {
                window.pupils = [];
                data.forEach(function(pupil) {
                    window.pupils.push({
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                            	pupil.lat,
                            	pupil.lon
                            ]
                        },
                        "properties": {
                            "title": "Pupile ID " + pupil.id,
                            "icon": "circle"
                        }
                    })
                });
                console.log("pupils load was performed");
                Array.prototype.push.apply(window.pupils,window.schools); 
                fetchMap();
            });
        }

        function fetchMap() {
            map
                .addLayer({
                    "id": "points",
                    "type": "symbol",
                    "source": {
                        "type": "geojson",
                        "data": {
                            "type": "FeatureCollection",
                            "features": window.pupils
                        }
                    },
                    "layout": {
                        "icon-image": "{icon}-15",
                        "text-field": "{title}",
                        "text-font": ["Open Sans Semibold",
                            "Arial Unicode MS Bold"
                        ],
                        "text-offset": [0, 0.6],
                        "text-anchor": "top"
                    }
                });
        }
    });
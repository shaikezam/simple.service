package com.simple.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pupils")
@NamedQuery(name = "Pupil.findAll", query = "SELECT p FROM Pupil p")
public class Pupil {

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@Column(name = "PUPIL_GRADES")
	private Set<Grade> grades;
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "FRIENDS")
	private Set<Long> friends;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PUPIL_ID", unique = true)
	private Long id;
	@Column(name = "PUPIL_LAT")
	private Double lat;

	@Column(name = "PUPIL_LON")
	private Double lon;

	public Long getId() {
		return id;
	}

	public Set<Grade> getGrades() {
		return grades;
	}

	public Double getLat() {
		return lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setGrades(Set<Grade> grades) {
		this.grades = grades;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Set<Long> getFriends() {
		return friends;
	}

	public void setFriends(Set<Long> friends) {
		this.friends = friends;
	}

	public double calculateGpa() {
		int gradesSum = 0;
		int gradesCount = grades.size();
		for (Grade grade : grades) {
			gradesSum += grade.getGrade();
		}
		return (gradesSum / gradesCount);
	}

	public void addFriend(long secondPupilId) {
		friends.add(secondPupilId);

	}

}

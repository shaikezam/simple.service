package com.simple.model;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "schools")
@NamedQuery(name = "School.findAll", query = "SELECT s FROM School s")
public class School {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "SCHOOL_ID", unique = true)
	private Long id;
	@Column(name = "SCHOOL_LAT")
	private Double lat;
	@Column(name = "SCHOOL_LON")
	private Double lon;
	@Column(name = "SCHOOL_MAX_NUMBER_OF_PUPILS")
	private Integer maxNumberOfPupils;
	@Column(name = "SCHOOL_MINIMUM_GPA")
	private Integer minimumGpa;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "PUPILS_IN_SCHOOL")
	private Set<Long> pupilsInSchool;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Integer getMaxNumberOfPupils() {
		return maxNumberOfPupils;
	}

	public void setMaxNumberOfPupils(Integer maxNumberOfPupils) {
		this.maxNumberOfPupils = maxNumberOfPupils;
	}

	public Integer getMinimumGpa() {
		return minimumGpa;
	}

	public void setMinimumGpa(Integer minimumGpa) {
		this.minimumGpa = minimumGpa;
	}

	public Set<Long> getPupilsInSchool() {
		return pupilsInSchool;
	}

	public void setPupilsInSchool(Set<Long> pupilsInSchool) {
		this.pupilsInSchool = pupilsInSchool;
	}

	public void addPupil(Long pupilId) {
		pupilsInSchool.add(pupilId);
	}

}

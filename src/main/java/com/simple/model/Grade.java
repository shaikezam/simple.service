package com.simple.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Grade {

	private String courseName;
	private int grade;
	@Id
	@GeneratedValue
	private int id;

	public String getCourseName() {
		return courseName;
	}

	public int getGrade() {
		return grade;
	}

	public int getId() {
		return id;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public void setId(int id) {
		this.id = id;
	}

}

package com.simple.repository;

import java.util.List;

import com.simple.model.School;

public interface SchoolRepository {
	School save(School school);

	School find(Long id);

	List<School> findAll();

	void addPupil(long selectedSchoolId, Long pupilId);
}

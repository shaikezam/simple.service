package com.simple.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.simple.model.School;

@Repository("schoolRepository")
public class SchoolRepositoryImpl implements SchoolRepository {
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public School save(School school) {
		em.persist(school);
		em.flush();
		return school;
	}

	@Override
	@Transactional
	public School find(Long id) {
		return em.find(School.class, id);
	}

	@Override
	@Transactional
	public List<School> findAll() {
		TypedQuery<School> query = em.createNamedQuery("School.findAll", School.class);
		List<School> schools = query.getResultList();
		return schools;
	}

	@Override
	@Transactional
	public void addPupil(long selectedSchoolId, Long pupilId) {
		School school = find(selectedSchoolId);
		school.addPupil(pupilId);
		em.merge(school);
		em.flush();

	}

}

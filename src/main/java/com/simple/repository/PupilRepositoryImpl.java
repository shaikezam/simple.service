package com.simple.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.simple.model.Pupil;

@Repository("pupilRepository")
public class PupilRepositoryImpl implements PupilRepository {
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public Pupil save(Pupil pupil) {
		em.persist(pupil);
		em.flush();
		return pupil;
	}

	@Override
	@Transactional
	public Pupil find(Long id) {
		return em.find(Pupil.class, id);
	}

	@Override
	@Transactional
	public void saveFriend(long firstPupilId, long secondPupilId) {
		Pupil pupil = find(firstPupilId);
		pupil.addFriend(secondPupilId);
		em.merge(pupil);
		em.flush();
	}

	@Override
	public List<Pupil> findAll() {
		TypedQuery<Pupil> query = em.createNamedQuery("Pupil.findAll", Pupil.class);
		List<Pupil> pupils = query.getResultList();
		return pupils;
	}

}

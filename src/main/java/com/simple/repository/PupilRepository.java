package com.simple.repository;

import java.util.List;

import com.simple.model.Pupil;

public interface PupilRepository {
	Pupil save(Pupil pupil);

	Pupil find(Long id);

	void saveFriend(long firstPupilId, long secondPupilId);

	List<Pupil> findAll();
}

package com.simple;

import java.io.IOException;
import java.net.BindException;
import java.net.URI;

import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	private static final URI BASE_URI = URI.create("http://localhost:8080/webapi");

	public static void main(String[] args) throws IOException {
		try {
			final JerseyConfig resourceConfig = new JerseyConfig();

			// load spring-jpa-context
			resourceConfig.property("contextConfig", new ClassPathXmlApplicationContext("jpa-context.xml"));
			// create stand-alone Jersey server
			final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, resourceConfig, false);

			// disable cache
			NetworkListener listener = server.getListener("grizzly");
			if (listener != null) {
				listener.getFileCache().setSecondsMaxAge(0);
			}
			server.addListener(listener);

			// serve static content
			server.getServerConfiguration()
					.addHttpHandler(new CLStaticHttpHandler(HttpServer.class.getClassLoader(), "/web/"), "/");

			server.start();
			System.out.println("Web api available at localhost:8080/webapi");
			System.out.println("Map web service available at localhost:8080");
			System.out.println(String.format("Jersey app available at %s\nHit enter to stop it...", BASE_URI));
			System.in.read();
			server.shutdownNow();
		} catch (BindException ex) {
			System.out.println("Port 8080 already taken, change the port or kill the process");
		}
	}
}

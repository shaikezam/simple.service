package com.simple.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simple.model.Pupil;
import com.simple.model.School;

@Service("enrollService")
public class EnrollServiceImpl implements EnrollService {

	@Autowired
	private SchoolService schoolService;

	@Autowired
	private PupilService pupilService;

	@Override
	public void enrollPupilToSchool(Long pupilId) {
		List<School> schools = schoolService.findAll();
		Pupil pupil = pupilService.find(pupilId);
		double gpa = pupil.calculateGpa();
		List<School> filteredSchools = filterSchool(schools, gpa);
		if (!filteredSchools.isEmpty()) {
			Map<Long, Double> distancesToSchools = getMapDistancesToSchools(filteredSchools, pupil);
			Map<Long, Integer> friendsInSchools = getMapFriendsInSchools(filteredSchools, pupil);
			long selectedSchoolId = findSchoolIdByDistanceAndFriends(distancesToSchools, friendsInSchools);
			schoolService.addPupil(selectedSchoolId, pupilId);
		}
	}

	// return schools that left room for new pupils and pupil GPA higher than min
	// school's GPA
	private List<School> filterSchool(List<School> schools, double gpa) {
		List<School> filteredSchools = new ArrayList<>();
		for (School school : schools) {
			int minimumGpa = school.getMinimumGpa();
			int maxNumberOfPupils = school.getMaxNumberOfPupils();
			int numberOfPupils = school.getPupilsInSchool().size();
			if (gpa > minimumGpa && numberOfPupils < maxNumberOfPupils) {
				filteredSchools.add(school);
			}
		}
		return filteredSchools;
	}

	// return map of schools id and distance to pupil
	private Map<Long, Double> getMapDistancesToSchools(List<School> schools, Pupil pupil) {
		Map<Long, Double> schoolsDistances = new HashMap<>();
		double pupilLat = pupil.getLat();
		double pupilLon = pupil.getLon();
		for (School school : schools) {
			long schoolId = school.getId();
			double schoolLat = school.getLat();
			double schoolLon = school.getLon();
			double distance = calculateHaversineFormula(pupilLat, schoolLat, pupilLon, schoolLon);
			schoolsDistances.put(schoolId, distance);
		}
		return schoolsDistances;
	}

	// return map of schools id and amount of pupil's friends in school
	private Map<Long, Integer> getMapFriendsInSchools(List<School> schools, Pupil pupil) {
		Map<Long, Integer> schoolsFriendsAmount = new HashMap<>();
		Set<Long> pupilFriends = pupil.getFriends();
		long pupilId = pupil.getId();
		for (School school : schools) {
			long schoolId = school.getId();
			Set<Long> pupilsInSchool = school.getPupilsInSchool();
			pupilsInSchool.retainAll(pupilFriends); // pupil friends in school
			for (Long id : pupilsInSchool) {
				Pupil pupilInSchool = pupilService.find(id);
				Set<Long> pupilInSchoolFriends = pupilInSchool.getFriends();
				if (!pupilInSchoolFriends.contains(pupilId)) {
					pupilsInSchool.remove(id);
				}
			}
			schoolsFriendsAmount.put(schoolId, pupilsInSchool.size());
		}
		return schoolsFriendsAmount;
	}

	// return the chosen school id by calculate the formula
	private long findSchoolIdByDistanceAndFriends(Map<Long, Double> distancesToSchools,
			Map<Long, Integer> friendsInSchools) {
		double val = 0;
		long selectedId = -1;
		Set<Long> schoolsIds = distancesToSchools.keySet(); // doesn't matter which map to take, they have same ID
		for (Long schoolId : schoolsIds) {
			double distance = distancesToSchools.get(schoolId);
			int friends = friendsInSchools.get(schoolId);
			double res = (friends * 1) / (distance);
			if (res >= val) {
				val = res;
				selectedId = schoolId;
			}
		}
		return selectedId;
	}

	// return the calculate distance between two points
	private double calculateHaversineFormula(double lat1, double lat2, double lon1, double lon2) {
		final int R = 6371; // Radius of the earth

		Double latDistance = toRad(lat2 - lat1);
		Double lonDistance = toRad(lon2 - lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double distance = R * c;
		return distance;
	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}

}

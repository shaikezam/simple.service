package com.simple.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simple.model.School;
import com.simple.repository.SchoolRepository;

@Service("schoolService")
public class SchoolServiceImpl implements SchoolService {

	@Autowired
	private SchoolRepository schoolRepository;

	@Override
	public School save(School school) {
		return schoolRepository.save(school);
	}

	@Override
	public School find(Long id) {
		return schoolRepository.find(id);
	}

	@Override
	public List<School> findAll() {
		return schoolRepository.findAll();
	}

	@Override
	public void addPupil(long selectedSchoolId, Long pupilId) {
		schoolRepository.addPupil(selectedSchoolId, pupilId);
	}

}

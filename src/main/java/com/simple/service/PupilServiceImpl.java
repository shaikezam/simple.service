package com.simple.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simple.model.Pupil;
import com.simple.repository.PupilRepository;

@Service("pupilService")
public class PupilServiceImpl implements PupilService {

	@Autowired
	private PupilRepository pupilRepository;

	@Override
	public Pupil save(Pupil pupil) {
		return pupilRepository.save(pupil);
	}

	@Override
	public Pupil find(Long id) {
		return pupilRepository.find(id);
	}

	@Override
	public void saveFriend(long firstPupilId, long secondPupilId) {
		pupilRepository.saveFriend(firstPupilId, secondPupilId);

	}

	@Override
	public List<Pupil> findAll() {
		return pupilRepository.findAll();
	}

}

package com.simple.service;

import java.util.List;

import com.simple.model.School;

public interface SchoolService {

	School save(School school);

	School find(Long id);

	List<School> findAll();

	void addPupil(long selectedSchoolId, Long pupilId);

}

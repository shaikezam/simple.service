package com.simple.service;

import java.util.List;

import com.simple.model.Pupil;

public interface PupilService {
	Pupil save(Pupil pupil);

	Pupil find(Long id);

	void saveFriend(long firstPupilId, long secondPupilId);

	List<Pupil> findAll();
}

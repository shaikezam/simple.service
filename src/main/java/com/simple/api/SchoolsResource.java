package com.simple.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.simple.model.School;
import com.simple.service.SchoolService;

@Path("schools")
public class SchoolsResource {

	@Autowired
	private SchoolService schoolService;

	// find all schools
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findSchools() {
		List<School> schools = schoolService.findAll();
		return Response.ok(schools).build();
	}
}

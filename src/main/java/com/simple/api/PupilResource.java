package com.simple.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.simple.model.Pupil;
import com.simple.service.PupilService;

@Path("pupil")
public class PupilResource {

	@Autowired
	private PupilService pupilService;

	// create new pupil
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response savePupil(Pupil pupil) {
		Pupil p = pupilService.save(pupil);
		return Response.ok(p.getId()).build();
	}

	// find pupils by id
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response findSchool(@PathParam("id") long id) {
		Pupil p = pupilService.find(id);
		return Response.ok(p).build();
	}
}

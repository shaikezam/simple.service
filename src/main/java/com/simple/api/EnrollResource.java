package com.simple.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.simple.service.EnrollService;

@Path("enroll")
public class EnrollResource {

	@Autowired
	private EnrollService enrollService;

	// enroll pupil to school
	@POST
	@Path("{pupilID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response savePupil(@PathParam("pupilID") long pupilId) {
		enrollService.enrollPupilToSchool(pupilId);
		return Response.noContent().build();
	}
}

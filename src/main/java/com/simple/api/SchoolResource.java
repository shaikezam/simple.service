package com.simple.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.simple.model.School;
import com.simple.service.SchoolService;

@Path("school")
public class SchoolResource {
	@Autowired
	private SchoolService schoolService;

	// create school
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveSchool(School school) {
		School s = schoolService.save(school);
		return Response.ok(s.getId()).build();
	}

	// find school by id
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response findSchool(@PathParam("id") long id) {
		School s = schoolService.find(id);
		return Response.ok(s).build();
	}
}

package com.simple.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.simple.service.PupilService;

@Path("setFriendShip")
public class FriendsResource {

	@Autowired
	private PupilService pupilService;

	// set secondPupilId to be friend of firstPupilId
	@POST
	@Path("{firstPupilId}/{secondPupilId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response savePupil(@PathParam("firstPupilId") long firstPupilId,
			@PathParam("secondPupilId") long secondPupilId) {
		pupilService.saveFriend(firstPupilId, secondPupilId);
		return Response.noContent().build();
	}

}

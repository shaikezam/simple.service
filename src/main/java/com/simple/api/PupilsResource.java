package com.simple.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.simple.model.Pupil;
import com.simple.service.PupilService;

@Path("pupils")
public class PupilsResource {
	@Autowired
	private PupilService pupilService;;

	// fetch pupils
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findPupils() {
		List<Pupil> schools = pupilService.findAll();
		return Response.ok(schools).build();
	}
}
